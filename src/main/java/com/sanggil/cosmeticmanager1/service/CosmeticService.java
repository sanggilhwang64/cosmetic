package com.sanggil.cosmeticmanager1.service;

import com.sanggil.cosmeticmanager1.entity.Cosmetic;
import com.sanggil.cosmeticmanager1.repository.CosmeticRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class CosmeticService {
    private final CosmeticRepository cosmeticRepository;

    public void setCosmetic (String name, String kind, String userName) {
        Cosmetic addData = new Cosmetic();
        addData.setName(name);
        addData.setKind(kind);
        addData.setUserName(userName);

        cosmeticRepository.save(addData);
    }
}
