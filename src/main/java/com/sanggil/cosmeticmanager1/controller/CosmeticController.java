package com.sanggil.cosmeticmanager1.controller;

import com.sanggil.cosmeticmanager1.model.CosmeticRequest;
import com.sanggil.cosmeticmanager1.service.CosmeticService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@RequestMapping("/cosmetic")
public class CosmeticController {
    private final CosmeticService cosmeticService;

    @PostMapping("/data")
    public String setCosmetic(@RequestBody CosmeticRequest request) {
        cosmeticService.setCosmetic(request.getName(), request.getKind(), request.getUserName());
        return "GOOD";
    }
}
