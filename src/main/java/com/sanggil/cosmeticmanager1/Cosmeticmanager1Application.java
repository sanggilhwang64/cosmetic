package com.sanggil.cosmeticmanager1;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Cosmeticmanager1Application {

    public static void main(String[] args) {
        SpringApplication.run(Cosmeticmanager1Application.class, args);
    }

}
