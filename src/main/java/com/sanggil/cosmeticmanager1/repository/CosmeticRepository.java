package com.sanggil.cosmeticmanager1.repository;

import com.sanggil.cosmeticmanager1.entity.Cosmetic;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CosmeticRepository extends JpaRepository<Cosmetic, Long> {
}
