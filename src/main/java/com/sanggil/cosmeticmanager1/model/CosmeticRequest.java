package com.sanggil.cosmeticmanager1.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CosmeticRequest {
    private String name;
    private String kind;
    private String userName;
}
